CREATE SCHEMA `inventory` DEFAULT CHARACTER SET utf8 ;

USE `inventory`;

CREATE TABLE `categories` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `location` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `items` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `category_id` INT NOT NULL,
  `location_id` INT NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `image` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_items_1_idx` (`category_id` ASC),
  INDEX `location_id_fk_idx` (`location_id` ASC),
  CONSTRAINT `category_id_fk`
    FOREIGN KEY (`category_id`)
    REFERENCES `inventory`.`categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `location_id_fk`
    FOREIGN KEY (`location_id`)
    REFERENCES `inventory`.`location` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

INSERT INTO `categories` (`title`, `description`)
VALUES
	("Furniture", "Some descr"),
	("Computers", "The most important things"),
	("Appliances", "Dish-washer and other things...");

INSERT INTO `location` (`title`, `description`)
VALUES
	("Director's ", "On the second floor, near toilet"),
	("Office 204", "The sicret place near the city"),
	("Teacher's", "The place, where teachers dring bear or vodka");

INSERT INTO `items` (`category_id`, `location_id`, `title`, `description`)
VALUES
	(2, 1, 'Laptop HP Probook 450', "Director's laptop"),
	(1, 1, 'Computer chair', "Chair from director's room"),
	(3, 2, 'Washing machine', "Our closes need to be clear!");