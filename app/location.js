const express = require('express');

const createRouter = connection => {
  const router = express.Router();

  router.get('/', (req, res) => {
    connection.query('SELECT * FROM `location`', (err, results) => {
      if (err) res.status(500).send({error: 'Database error'});
      res.send(results);
    });
  });

  router.get('/:id', (req, res) => {
    connection.query('SELECT * FROM `location` WHERE id = ?',
      req.params.id,
      (err, results) => {
        if (err) res.status(500).send({error: 'Database error'});
        if (results[0]) {
          res.send(results[0]);
        } else {
          res.status(404).send({error: 'location not found'});
        }
      }
    );
  });

  router.post('/', (req, res) => {
    const location = req.body;
    connection.query('INSERT INTO `location` (`title`, `description`) VALUES (?, ?)',
      [location.title, location.description],
      (err, results) => {
        if (err) res.status(500).send({error: 'Database error'});
        res.send({message: 'OK'});
      }
    );
  });

  router.delete('/:id', (req, res) => {
    connection.query('SELECT * FROM `items` WHERE `location_id` = ?',
      req.params.id,
      (err, results) => {
        if (err) res.status(500).send({error: 'Database error'});

        if (!results.length) {
          connection.query('DELETE FROM `location` WHERE `id` = ?',
            req.params.id,
            (err, results) => {
              if (err) res.status(500).send({error: 'Database error'});
              res.send({message: "OK, deleted"});
            }
          )
        } else {
          res.send({message: "Error, this category contains related resources"});
        }
      }
    );
  });

  return router;
};

module.exports = createRouter;