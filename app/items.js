const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

// configurations for upload files (images)
const storage = multer.diskStorage({
  description: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const createRouter = connection => {
  const router = express.Router();

  router.get('/', (req, res) => {
    connection.query('SELECT `id`, `category_id`, `location_id`, `title` FROM `items`', ((err, results) => {
      if (err) {
        res.status(500).send({error: 'Database error'});
      }

      res.send(results);
    }))
  });

  router.get('/:id', (req, res) => {
    connection.query('SELECT * FROM `items` WHERE `id` = ?',
      req.params.id,
      (err, results) => {
        if (err) {
          res.status(500).send({error: 'Database error'});
        }

        if (results[0]) {
          res.send(results[0]);
        } else {
          res.status(404).send({error: 'Resource not found'});
        }
      });
  });

  router.post('/', upload.single('image'), (req, res) => {
    const resource = req.body;
    if (req.file) resource.image = req.file.filename;

    connection.query('INSERT INTO `items` (`category_id`, `location_id`, `title`, `description`, `image`) VALUES (?, ?, ?, ?, ?)',
      [resource.category_id, resource.location_id, resource.title, resource.description, resource.image],
      (err, results) => {
        if (err) {
          res.status(500).send({error: err});
        }

        res.send({message: 'OK'});
      }
    );
  });

  router.delete('/:id', (req, res) => {
    connection.query('DELETE FROM `items` WHERE `id` = ?',
      req.params.id,
      (err, results) => {
        if (err) res.status(500).send(err);

        res.send({message: 'OK, deleted'});
      });
  });

  return router;
};

module.exports = createRouter;