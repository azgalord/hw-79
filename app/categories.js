const express = require('express');

const createRouter = connection => {
  const router = express.Router();

  router.get('/', (req, res) => {
    connection.query('SELECT `id`, `title` FROM `categories`', (err, results) => {
      if (err) res.status(500).send({error: 'Database error'});

      res.send(results);
    })
  });

  router.get('/:id', (req, res) => {
    connection.query('SELECT * FROM `categories` WHERE `id` = ?',
      req.params.id,
      (err, results) => {
        if (err) res.status(500).send({error: 'Database error'});

        if (results[0]) {
          res.send(results[0]);
        } else {
          res.status(404).send({error: 'Resource not found'});
        }
      });
  });

  router.post('/', (req, res) => {
    const category = req.body;
    connection.query('INSERT INTO `categories` (`title`, `description`) VALUES (?, ?)',
      [category.title, category.description],
      (err, results) => {
        if(err) res.status(500).send(err);
        res.send({message: 'OK'});
      }
    );
  });

  router.delete('/:id', (req, res) => {
    connection.query('SELECT * FROM `items` WHERE `category_id` = ?',
      req.params.id,
      (err, results) => {
        if (err) res.status(500).send({error: 'Database error'});

        if (!results.length) {
          connection.query('DELETE FROM `categories` WHERE `id` = ?',
            req.params.id,
            (err, results) => {
              if (err) res.status(500).send({error: 'Database error'});
              res.send({message: "OK, deleted"});
            }
          );
        } else {
          res.send({message: "Error, this category contains related resources"});
        }
      });
  });

  return router;
};

module.exports = createRouter;