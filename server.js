const express = require('express');
const cors = require('cors');
const mysql = require('mysql');

const items = require('./app/items');
const categories = require('./app/categories');
const location = require('./app/location');

const port = 8000;
const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  database: 'inventory'
});

app.use('/items', items(connection));
app.use('/categories', categories(connection));
app.use('/location', location(connection));

connection.connect(err => {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }
  console.log('connected as id ' + connection.threadId);

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});




